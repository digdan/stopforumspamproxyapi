// http://www.stopforumspam.com/downloads/listed_ip_365.zip
var debug = require('debug')('importer');
var progress = require('progress');
var http = require('http');
var fs = require('fs');
var csv = require('fast-csv');
var admZip = require('adm-zip');
var redis = require("redis");
var client = redis.createClient();


client.on("error", function(err) {
	console.log("Error "+ err);
});

function fromDownload(url,cb) {
	var request = http.get(url,function(res) {
		var downloadLength = parseInt(res.headers['content-length'],10);
		var data=[];
		var dataLen = 0;
		debug('Downloading full records size : '+downloadLength);
		var downloadBar = new progress('Downloading [:bar] :percent :etas', {
			complete: '=',
			incomplete:'-',
			width:100,
			total:downloadLength
		});
		res.on("data", function(chunk) {
			downloadBar.tick(chunk.length);
			data.push(chunk);
			dataLen += chunk.length;
		});
		res.on("end", function() {
			debug('Download completed.');
			parseData(data,cb)
		});
		
	});
}

function fromFile(file,cb) {
	debug('Parsing zipdata from '+file);
	fs.readFile(file,function (err,data) {
		debug('File read completed.');
		parseData(data,cb);
	});
}

function parseData(data,cb) {
	debug('Started parsing records');
	var zip = new admZip(data);
	var zipEntries = zip.getEntries();
	var dataString = zip.readAsText(zipEntries[0]);
	var dataLength = dataString.split(/\r\n|\r|\n/).length;
	var ic=1;
	debug('Found '+dataLength+' records');
	var dbBar = new progress('Parsing records [:bar] :percent :current/:total (:eta)',{
		complete: '=',
		incomplete:'-',
		width:100,
		total:dataLength
	});
	csv
	.fromString( dataString )
	.on("data", function(data) {
		var hkey = 'ip:'+data[0];
		client.hmset([hkey,"count",data[1],"updated",data[2]]);
		client.expire(hkey,(60*60*24*365));
		dbBar.tick();
		ic++;
		if (ic >= dataLength) cb(null);
	})
}

debug('Importer started.');
//SFP uses redis db 2
client.select(2, function() {
	fromDownload('http://www.stopforumspam.com/downloads/listed_ip_1_all.zip', function() {
		debug('Import Completed.');
		process.exit();
	});
});
	
