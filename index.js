const debug = require('debug')('sfp');
var http = require('http');
var https = require('https');
var cluster = require('cluster');
var redis = require('redis');


// Main Application

debug('Master Init.');

if (cluster.isMaster) {
	debug('Booting ');
	for (var i = 0; i < require('os').cpus().length; i += 1) cluster.fork();
} else {
	var express = require('express');
	var app = express();
	var client = redis.createClient();	
	
	app.get('/', function(req,res) {
		debug('Connected to worker '+cluster.worker.id);
		var ip = Object.keys(req.query)[0];
		var hkey = 'ip:'+ip;
		client.hgetall(hkey, function( error, obj ) {
			if (obj == null) {
				debug('Worker '+cluster.worker.id+' looked up IP '+ip+' - no results ');
				res.send(JSON.stringify(false));
			} else {
				debug('Worker '+cluster.worker.id+' looked up IP '+ip+' results returned ');
				res.send(JSON.stringify(obj));
			}
		});
	});

	client.select(2,function() {
		debug('Worker ', cluster.worker.id ,' connected to db.');
		app.listen(3000);
		debug('Worker ', cluster.worker.id ,' inited.');
	});
}

cluster.on('exit', function(worker) {
	debug('Worker ',cluster.worker.id, ' exited.');	
});
